#!/usr/bin/env python
from __future__ import print_function
try:
	range=xrange
except:
	pass

import sys
import os
import time
from datetime import timedelta
start_clock = time.clock()

import logging
logging.basicConfig(format='[%(levelname).4s] %(message)s', level=logging.INFO)

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 1001;")

from array import array

class Selection:
	"""
	A selection object defines a filter for events (i.e. selects events
	that satisfy some requirements `cuts`). It can optionally derive
	from another Selection `base`.
	Histograms booked for this Selection object will be filled with
	the given `weight` by default.
	"""
	def __init__(self, name, cuts, weight='weight', base=None):
		self.name = name
		self.cuts = cuts
		self.weight = weight
		self.base = base

		self.histograms = []

		self.filter = None

	def create(self, df):
		if self.filter:
			return

		filt = df

		if self.base:
			self.base.create(df)
			filt = self.base.filter

		for cut in self.cuts[:-1]:
			filt = filt.Filter(cut)
		self.filter = filt.Filter(self.cuts[-1], self.name)

		# directly book a histogram that just counts events
		self.histograms = [
			self.filter.Histo1D(("count", "", 1, 0, 2), "_1", self.weight)
		]


	def book(self, model, var, weight='_default', stats=False):
		if weight == '_default':
			weight = self.weight
		if len(model) == 3:
			model = (model[0], model[1], len(model[2])-1, array('f', model[2]))

		f = self.filter

		# Histo1D and variants can only take existing columns
		# If we dont find a column with that name, interpret `var` as
		# an expression
		if var not in self.filter.GetColumnNames():
			expr = var
			var = self._sanitize(expr)
			f = self.filter.Define(var, expr)


		self.histograms.append(f.Histo1D(model, var, weight))

	def store(self, output):
		outdir = output.mkdir(self.name)
		for i, hist in enumerate(self.histograms):
			h = hist.GetValue()
			outdir.WriteTObject(h)

	def _sanitize(self, expr):
		badchars = r'.()[]{}:/\+-*'

		for c in badchars:
			expr = expr.replace(c, '_')

		return expr

class Analysis:
	"""
	An analysis is a collection of Selection objects.
	"""
	def __init__(self):
		self.selections = {}
	
	def add(self, name, cuts, weight='_default', base=None):
		if type(cuts) == str:
			cuts = [cuts]

		if base:
			if type(base) == str:
				base = self.selections[base]

			if weight == '_default':
				weight = base.weight

		if weight == '_default':
			weight = 'weight'

		sel = Selection(name, cuts, weight, base=base)
		self.selections[name] = sel

		return sel

	def create(self, df):
		for sel in self.selections.values():
			sel.create(df)

	def book(self, model, var, weight='_default', stats=False):
		for sel in self.selections.values():
			sel.book(model, var, weight, stats=stats)

	def book_opt(self, model, var, weight='_default'):
		for sel in self.selections.values():
			sel.book_opt(model, var, weight)

	def book_postprocess(self, proc, model, var, weight='_default'):
		for sel in self.selections.values():
			sel.book_postprocess(proc, model, var, weight)

	def store(self, output):
		for sel in self.selections.values():
			sel.store(output)

def add_defines(df):
	# Add some extra columns to the dataset, that summarise the ntuple values
	# to make them easier to use

	# Some helper functions to create string representations of code

	# 4-vector object
	# See: https://root.cern.ch/doc/master/classTLorentzVector.html
	# Already transform the pT and energy from MeV (in the ntuple) to GeV
	def _p4(obj, index):
		return """
			TLorentzVector v4; 
			v4.SetPtEtaPhiE({obj}_pt{index}*1e-3, {obj}_eta{index}, {obj}_phi{index}, {obj}_e{index}*1e-3); 
			return v4;
			""".format(obj=obj, index=index)

	# The "transverse mass" of an object (lepton) with the missing transverse
	# momentum
	def _mt(lep):
		return """
		std::sqrt(2 * {lep}.Pt() * met_gev * (1 - std::cos({lep}.Phi() - met_phi)));
		""".format(lep=lep)


	df = (df
		.Define("lep0", _p4('lep', 0))
		.Define("lep1", _p4('lep', 1))
		.Define("jet0", _p4('jet', 0))
		.Define("jet1", _p4('jet', 1))

		.Define("dilep", "lep0+lep1")

		.Define("m_lj", "(jet0.DeltaR(lep0) < jet0.DeltaR(lep1)) ? (jet0 + lep0).M() : (jet0 + lep1).M()")

		.Define("weight_bjets", "weight*btag_weight")

		.Define("_1", "1") # some parts require column names and cannot work with constants
	)

	return df


def define_selections(df):
	logging.info("Creating analysis selections")
	ana = Analysis()

	# Give the baseline selection a nice name
	base = ana.add('CutSRBase', '1==1', 'weight')

	# The ntuples are created with m_ll > 45 GeV (for other studies)
	presel = ana.add("CutSRHiggsRemoval", 'dilep.M() > 85', 'weight', base=base) 

	# Split according to number of bjets
	# SR: "Signal region" 0 b-jets
	# CR1b, CR2b: "Control region" with 1/2 b-jets
	# BCR: "b-jet control region" any number of bjets
	# For selections and plots that impose any requirements on b-jets, the
	# 'weight_bjets' has to be used, otherwise only 'weigth'
	nbjet_selections = [
		('SR', ana.add('CutSRBVeto', 'n_bjet==0', 'weight_bjets', base=presel)),
		('CR1b', ana.add('CutCR1b', 'n_bjet==1', 'weight_bjets', base=presel)),
		('CR2b', ana.add('CutCR2b', 'n_bjet==2', 'weight_bjets', base=presel)),
		('BCR', ana.add('CutBCR', '1==1', 'weight', base=presel)),
	]

	# Now add the jet selection for any of the regions defined above
	for region_id, bjet_selection in nbjet_selections:
		ana.add('Cut%sOneJetIncl' % region_id, 'n_jet_30>=1', base=bjet_selection),

	ana.create(df)
	return ana

def book_histograms(ana):
	logging.info("Booking histograms")

	# Book some histograms to be filled for each event
	# Parameters are: histogram-model, variable to fill, weight (optional, default='weight')
	# The histogram model is a tupel of
	# 	the name
	# 	the title ('hist title;x-axis title;y-axis title')
	#	number of bins
	# 	x-min
	#	x-max
	# Alternatively the binning can be given as a list of bin edges

	ana.book(('n_jet', ';Jet multiplicity (30 GeV);Events', 8, -0.5, 7.5), 'n_jet_30')
	ana.book(('n_bjet', ';b-jet multiplicity (20 GeV);Events', 5, -0.5, 4.5), 'n_bjet', weight='weight_bjets')

	ana.book(('pt_lep0', ';Leading lepton p_{T} [GeV];Events', 45, 0, 450), 'lep0.Pt()')
	ana.book(('lep_pt_r', ';p_{T,sub-lead. lep} / p_{T,lead. lep}', 100, 0., 1.0), 'lep1.Pt()/lep0.Pt()')


def make_rdf(tree):
	df_base = ROOT.RDataFrame(tree)
	counter = df_base.Count()
	df = add_defines(df_base)

	ana = define_selections(df)

	book_histograms(ana)

	return df_base, ana, counter

def load_tree(filepath):
	# A chain supports multiple files, in case that is ever needed
	chain = ROOT.TChain('WWNtuple')
	chain.Add(filepath)

	logging.info("Chain with %d files", chain.GetNtrees())
	return chain

def parse_args():
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument('--mt', dest='mt', type=int, default=0, help='Enable multithreading')
	parser.add_argument('--no-mt', dest='mt', action='store_const', const=-1, help='Disable multithreading')

	parser.add_argument("-i", "--input", required=True, help="Input .root file with ntuples")
	parser.add_argument("-o", "--output", required=True, help="Output file")

	args = parser.parse_args()

	return args

def main():
	args = parse_args()

	if args.mt >= 0:
		if args.mt == 0:
			logging.info("Multithreading with all cores")
			ROOT.ROOT.EnableImplicitMT()
		else:
			logging.info("Multithreading with %d cores" % args.mt)
			ROOT.ROOT.EnableImplicitMT(args.mt)
	else:
		logging.info("Multithreading disabled")

	output_dir = os.path.dirname(args.output)
	if not os.path.isdir(output_dir):
		try: 
			os.makedirs(output_dir)
		except OSError: # race conditions
			if not os.path.isdir(output_dir):
				raise

	logging.info("Loading ntuple from %s", args.input)
	tree = load_tree(args.input)
	df, analysis, counter = make_rdf(tree)
	report = df.Report()

	tmp_path = args.output + '.tmp'
	logging.info("Writing to %s" % (tmp_path))
	output = ROOT.TFile.Open(tmp_path, 'recreate')

	logging.info("Executing event loop")
	# this triggers the lazy event loop
	analysis.store(output)


	output.Close()
	os.rename(tmp_path, args.output)
	logging.info("Output moved to %s" % (args.output))


	end_clock = time.clock()
	total_seconds = end_clock - start_clock
	dt = timedelta(seconds=total_seconds)
	logging.info('Total events: {}'.format(counter.GetValue()))
	logging.info('Total runtime: {}'.format(dt))
	logging.info('%d events/second processed (incl. initialization)' % (counter.GetValue() / total_seconds))

if __name__ == '__main__':
	main()
