#!/usr/bin/env python
from __future__ import print_function
try:
	range=xrange
except:
	pass

import sys
import os

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 1001;")


def parse_args():
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("input", help="Input .root file with histograms")
	parser.add_argument("selection", help="For which cut?")

	args = parser.parse_args()

	return args

def main():
	args = parse_args()

	infile = ROOT.TFile.Open(args.input)

	# the make-histograms.py script automatically creates
	# a 'count' histogram for every selection
	counthist = infile.Get('{}/count'.format(args.selection))

	# The count histogram only has one bin, in which every selected
	# event is entered
	num_events = counthist.GetBinContent(1) # 1st bin has number 1 (0 = 'underflow')

	# Also get the stat. error in the event count:
	error = counthist.GetBinError(1)

	# If every event would have the same weight, then error == sqrt(num_events) (Poisson)
	# but we have weighted events, so we use the ROOT internal mechanism, which tracks
	# the event weights and calculates the sum of the squared weights to derive the
	# uncertainty.
	print('Events: %d +/- %d' % (num_events, error))

if __name__ == '__main__':
	main()