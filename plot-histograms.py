#!/usr/bin/env python
from __future__ import print_function
try:
	range=xrange
except:
	pass

import sys
import os

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 1001;")

# nicer than the ROOT default
import PlotStyle

def load_histogram(filepath, selection, histname):
	fp = ROOT.TFile.Open(filepath)

	hist = fp.Get('{}/{}'.format(selection, histname))

	if not hist:
		print("Error: could not load '{}/{}' from '{}'".format(selection, histname, filepath))

	# prevent ROOT from 'helpfully' deleting the
	# histogram when closing the file
	hist.SetDirectory(ROOT.gROOT)

	fp.Close()

	return hist

def parse_args():
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("input", help="Input .root file with histograms")
	parser.add_argument("selection", help="For which cut?")
	parser.add_argument("hist", help="The histogram to plot")

	args = parser.parse_args()

	return args

def main():
	args = parse_args()


	hist = load_histogram(args.input, args.selection, args.hist)

	hist.SetLineColor(ROOT.kBlack)
	hist.SetLineWidth(2)
	hist.SetFillColor(ROOT.kGray+1)

	# Now plot everything
	canv = ROOT.TCanvas('canv', '', 800, 600)

	hist.Draw('hist')
	canv.RedrawAxis()

	name = '%s-%s-%s' % (os.path.basename(args.input).replace('.root', ''), args.selection, args.hist)
	canv.SaveAs('plots/%s.pdf' % name)
	canv.SaveAs('plots/%s.png' % name)


if __name__ == '__main__':
	main()