# Ntuple format

The data are stored in a [ROOT](https://root.cern.ch/) TTree format, which contains one entry per simulated event.
Many more events than expected in data are simulated (to reduce the statistical uncertainty),
and the `weight` value can be used to translate between simulated and expected events.

The following simulated datasets are available:
- `WW.root`: Pair production of two W-bosons (quark anti-quark induced), with leptonic decays of the W bosons (this is the process that we want to measure)
- `ttbar.root`: Top quark pair production, with leptonic decays of the two top quarks (`t -> W b -> lepton neutrino b`). This is the most important "background" process.
- `singletop_Wt.root`: Single top quark production with an associated W-boson, with leptonic decays of the top quark and the W boson
- `Zjets.root`: Z-boson production with the decay `Z + X-> tau tau + X -> electron muon + X`


## "Branches" (values) in the TTree
Some values in the files do not hold usable information (artifacts for other selections, older versions), marked as `do not use` below.

```
TTree WWNtuple
     Int_t	run_number		# Data is taken in "Runs" (up to ~24h), simulation is structured similarly
 ULong64_t	event_number		# Unique identifier (per run) for this event
     Int_t	mc_channel_number	# Internal ID of the simulated dataset
     Int_t	n_pv			# Number of primary vertices (~= simultaneous p-p collisions )
   Float_t	scaled_mu		# Average number of simultaneous p-p collisions
   Float_t	met 			# Missing transverse energy in MeV -- loosely proportional to the total  transverse momentum of all neutrinos in the event
   Float_t	met_phi			# Angle (in x/y) of the met
   Float_t	ht			# H_T: scalar sum of all jet transverse momenta in MeV
   Float_t	mll3			# do not use
     Int_t	vy_overlap		# do not use
     Int_t	n_jet 			# Number of hadronic jets (pT > 30 GeV)
     Int_t	n_bjet                  # Number of b-tagged jets
     Int_t	n_bjet_truth            # True number of b-tagged jets in the simulation
   Float_t	btag_weight             # Extra weight to be multiplied with `weight` when using any requirements on b-jets
     Int_t	n_jet_20		# Number of hadronic jets (pT > 20 GeV)
     Int_t	n_jet_25		# ...
     Int_t	n_jet_30
     Int_t	n_jet_35
     Int_t	n_jet_40
   Float_t	lep_pt0			# Transverse momentum of the pT-leading lepton in MeV
   Float_t	lep_eta0		# Pseudo-rapidity of the pT-leading lepton
   Float_t	lep_phi0		# Phi-angle (in x/y) of the pT-leading lepton
   Float_t	lep_e0 			# Energy of the pT-leading lepton in MeV
     Int_t	lep_charge0		# Charge of the pT-leading lepton
     Int_t	lep_is_el0		# 1 if the leading lepton is an electron, 0 if it is a muon
   Float_t	lep_pt1			# Same fields for second lepton (pT ordered)
   Float_t	lep_eta1
   Float_t	lep_phi1
   Float_t	lep_e1
     Int_t	lep_charge1
     Int_t	lep_is_el1
   Float_t	jet_pt0			# Same fields (no charge/type info) for leading jet (pT orderd)
   Float_t	jet_eta0		# Field are only usable if n_jet > 0
   Float_t	jet_phi0
   Float_t	jet_e0
     Int_t	jet_truthlabel0		# True source of the jet (5=b-jet)
     Int_t	jet_mv2c10tag0		# do not use
   Float_t	jet_pt1			# Same fields (no charge/type info) for second jet (pT orderd)
   Float_t	jet_eta1		# Field are only usable if n_jet > 1
   Float_t	jet_phi1
   Float_t	jet_e1
     Int_t	jet_truthlabel1
     Int_t	jet_mv2c10tag1
   Float_t	jet_pt2			# Same fields (no charge/type info) for third jet (pT orderd)
   Float_t	jet_eta2		# Field are only usable if n_jet > 2
   Float_t	jet_phi2
   Float_t	jet_e2
     Int_t	jet_truthlabel2
     Int_t	jet_mv2c10tag2
  Double_t	weight 			# Event weight, translates between simulated and expected events
```

Events stored in the files fulfil most of the WW measurement selection criteria:
- 1 electron and 1 muon of opposite charge
- lepton (muon/electron) reconstruction and identification quality requirements
- lepton (muon/electron) pT > 27 GeV (lowest value possible after trigger requirements)
- Invariant mass of the pair of leptons (m_ll) > 45 GeV

For the full WW measurement selection the following requirements need to be made:
- Invariant mass of the pair of leptons (m_ll) > **85 GeV**
- b-jet veto: `n_bjet == 0`
- Jet requirement: `n_jet >= 1`

## `make-histograms.py`

The `make-histograms.py` can used to apply an event selection and make histograms for selected events.

Usage:
```
make-histograms.py -i <input root file> -o <output file>
```

The script requires [`ROOT`](https://root.cern.ch/) to be set up. On the DESY NAF you can use the `setup-naf.sh` script to initialize the environment variables needed:
```
source setup-naf.sh
```

The script uses the ROOT [RDataFrame](https://root.cern/doc/v616/classROOT_1_1RDataFrame.html) to process
the inputs.

## `plot-histograms.py`

Plot a histogram from a histogram file.

## `print-event-counts.py`

Print the number of selected events from a histogram file



